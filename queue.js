let collection = [];
// Write the queue functions below.

function enqueue(element) {
    collection[collection.length] = element
    return collection
}

function print() {
    return collection
}


function dequeue() {
     for (let i = 0; i < collection.length; i++){
        collection[i] = collection[i+1]
    }
    collection.length = collection.length-1
    return collection
}

function front() {
    return collection[0]
}

function size() {
    return collection.length
}

function isEmpty() {
   return collection.length == 0
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
